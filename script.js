function oneThroughTwenty() {
  const numbers = [];
  const newDiv = document.createElement("div");

  // Your code goes below
  for (let i = 1; i <= 20; i++) {
    numbers.push(i);
  }
  const content = document.createTextNode(numbers.toString());
  newDiv.appendChild(content);
  document.getElementById("one").appendChild(newDiv);
}

oneThroughTwenty();

////////////////////////////////////////////////////////////

function evensToTwenty() {
  const numbers = [];
  const newDiv = document.createElement("div");

  // Your code goes below
  for (let i = 1; i <= 20; i++) {
    if (i % 2 === 0) {
      numbers.push(i);
    }
  }
  const content = document.createTextNode(numbers.toString());
  newDiv.appendChild(content);
  document.getElementById("two").appendChild(newDiv);
}

evensToTwenty();

///////////////////////////////////////////////////////////

function oddsToTwenty() {
  const numbers = [];
  const newDiv = document.createElement("div");

  // Your code goes below
  for (let i = 1; i <= 20; i++) {
    if (i % 2 != 0) {
      numbers.push(i);
    }
  }
  const content = document.createTextNode(numbers.toString());
  newDiv.appendChild(content);
  document.getElementById("three").appendChild(newDiv);
}

oddsToTwenty();

//////////////////////////////////////////////////////////

function multiplesOfFive() {
  const numbers = [];
  const newDiv = document.createElement("div");

  // Your code goes below
  for (let i = 1; i <= 100; i++) {
    if (i % 5 === 0) {
      numbers.push(i);
    }
  }

  const content = document.createTextNode(numbers.toString());
  newDiv.appendChild(content);
  document.getElementById("four").appendChild(newDiv);
}

multiplesOfFive();

////////////////////////////////////////////////////////////

function squareNumbers() {
  const numbers = [];
  const newDiv = document.createElement("div");

  // Your code goes below
  for (let i = 1; i <= 100; i++) {
    let v = i * i;

    if (v <= 100) {
      numbers.push(v);
    }
  }

  const content = document.createTextNode(numbers.toString());
  newDiv.appendChild(content);
  document.getElementById("five").appendChild(newDiv);
}

squareNumbers();

///////////////////////////////////////////////////////////

function countingBackwards() {
  const numbers = [];
  const newDiv = document.createElement("div");

  // Your code goes below
  for (let i = 20; i >= 1; i--) {
    numbers.push(i);
  }

  const content = document.createTextNode(numbers.toString());
  newDiv.appendChild(content);
  document.getElementById("six").appendChild(newDiv);
}

countingBackwards();

/////////////////////////////////////////////////////////

function evenNumbersBackwards() {
  const numbers = [];
  const newDiv = document.createElement("div");

  // Your code goes below
  for (let i = 20; i >= 1; i--) {
    if (i % 2 === 0) {
      numbers.push(i);
    }
  }

  const content = document.createTextNode(numbers.toString());
  newDiv.appendChild(content);
  document.getElementById("seven").appendChild(newDiv);
}

evenNumbersBackwards();

//////////////////////////////////////////////////////////////

function oddNumbersBackwards() {
  const numbers = [];
  const newDiv = document.createElement("div");

  // Your code goes below
  for (let i = 20; i >= 1; i--) {
    if (i % 2 != 0) {
      numbers.push(i);
    }
  }

  const content = document.createTextNode(numbers.toString());
  newDiv.appendChild(content);
  document.getElementById("eight").appendChild(newDiv);
}

oddNumbersBackwards();

//////////////////////////////////////////////////////////////

function multiplesOfFiveBackwards() {
  const numbers = [];
  const newDiv = document.createElement("div");

  // Your code goes below
  for (let i = 100; i >= 1; i--) {
    if (i % 5 === 0) {
      numbers.push(i);
    }
  }

  const content = document.createTextNode(numbers.toString());
  newDiv.appendChild(content);
  document.getElementById("nine").appendChild(newDiv);
}

multiplesOfFiveBackwards();

//////////////////////////////////////////////////////////////

function squareNumbersBackwards() {
  const numbers = [];
  const newDiv = document.createElement("div");

  // Your code goes below
  for (let i = 100; i >= 1; i--) {
    let v = i * i;

    if (v <= 100) {
      numbers.push(v);
    }
  }

  const content = document.createTextNode(numbers.toString());
  newDiv.appendChild(content);
  document.getElementById("ten").appendChild(newDiv);
}

squareNumbersBackwards();

///////////////////////////////////////////////////////////

const sampleArray = [
  469,
  755,
  244,
  245,
  758,
  450,
  302,
  20,
  712,
  71,
  456,
  21,
  398,
  339,
  882,
  848,
  179,
  535,
  940,
  472
];

///////////////////////////////////////////////

function sampleArray20() {
  const newDiv = document.createElement("div");
  const content = document.createTextNode(sampleArray.toString());
  newDiv.appendChild(content);
  document.getElementById("eleven").appendChild(newDiv);
}

sampleArray20();

///////////////////////////////////////////////////////////////////

function sampleArrayEvens() {
  const numbers = [];
  const newDiv = document.createElement("div");

  for (let i = 0; i < sampleArray.length; i++) {
    if (sampleArray[i] % 2 === 0) {
      numbers.push(sampleArray[i]);
    }
  }
  const content = document.createTextNode(numbers.toString());
  newDiv.appendChild(content);
  document.getElementById("twelve").appendChild(newDiv);
}

sampleArrayEvens();

///////////////////////////////////////////////////////////////

function sampleArrayodds() {
  const numbers = [];
  const newDiv = document.createElement("div");

  for (let i = 0; i < sampleArray.length; i++) {
    if (sampleArray[i] % 2 !== 0) {
      numbers.push(sampleArray[i]);
    }
  }
  const content = document.createTextNode(numbers.toString());
  newDiv.appendChild(content);
  document.getElementById("thirteen").appendChild(newDiv);
}

sampleArrayodds();

////////////////////////////////////////////////////////////////

function SampleArraySquareNumbers() {
  const numbers = [];
  const newDiv = document.createElement("div");

  // Your code goes below
  for (let i = 0; i < sampleArray.length; i++) {
    let v = sampleArray[i] * sampleArray[i];
    numbers.push(v);
  }

  const content = document.createTextNode(numbers.toString());
  newDiv.appendChild(content);
  document.getElementById("fourteen").appendChild(newDiv);
}

SampleArraySquareNumbers();

//////////////////////////////////////////////////////////////////

function Sum1to20() {
  let sum = 0;
  const newDiv = document.createElement("div");

  // Your code goes below
  for (let i = 0; i <= 20; i++) {
    sum += i;
  }

  const content = document.createTextNode(sum.toString());
  newDiv.appendChild(content);
  document.getElementById("fifteen").appendChild(newDiv);

  console.log(sum);
}

Sum1to20();

//////////////////////////////////////////////////////////////////

function SampleArraySum1to20() {
  let sum = 0;
  const newDiv = document.createElement("div");

  // Your code goes below
  for (let i = 0; i < sampleArray.length; i++) {
    sum += sampleArray[i];
  }

  const content = document.createTextNode(sum.toString());
  newDiv.appendChild(content);
  document.getElementById("sixteen").appendChild(newDiv);

  console.log(sum);
}

SampleArraySum1to20();

/////////////////////////////////////////////////////////

function sampleArraySmallest() {
  const newDiv = document.createElement("div");
  const content = document.createTextNode(Math.min(...sampleArray));
  newDiv.appendChild(content);
  document.getElementById("seventeen").appendChild(newDiv);
  console.log(Math.min(...sampleArray));
}

sampleArraySmallest();

/////////////////////////////////////////////////////////

function sampleArrayMax() {
  const newDiv = document.createElement("div");
  const content = document.createTextNode(Math.max(...sampleArray));
  newDiv.appendChild(content);
  document.getElementById("eighteen").appendChild(newDiv);
}

sampleArrayMax();
